algoliasearch-django>=2.0,<3.0
django==4.0.4
djangorestframework
djangorestframework-simplejwt
pyyaml
requests
django_cors_headers