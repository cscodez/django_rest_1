from algoliasearch_django import AlgoliaIndex
from algoliasearch_django.decorators import register

from .models import Product


@register(Product)
class ProductIndex(AlgoliaIndex):
    # should_index = 'is_public'  # updating indexes based on its value (True/False)
    fields = [
        'title',
        'body',
        'price',
        'user',
        'public',
        'path',
        'endpoint',
    ]
    settings = {
        'searchableAttributes': ['title', 'body'],
        'attributesForFaceting': ['user', 'public']
    }
    tags = 'get_tags_list'  # you give the function name that is responsible for giving a tag

# It's similar to admin.site.register(Product, ProductModelAdmin)
# run python manage.py algolia_reindex
