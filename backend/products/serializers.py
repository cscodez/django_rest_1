from abc import ABC

from rest_framework import serializers
from rest_framework.reverse import reverse

from api.serializers import UserPublicSerializer

from .models import Product
from . import validators


class ProductInlineSerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='product-detail',
        lookup_field='pk',
        read_only=True
    )
    title = serializers.CharField(read_only=True)


class ProductSerializer(serializers.ModelSerializer):  # exactly same like forms.ModelForm just with serializers
    owner = UserPublicSerializer(source='user', read_only=True)

    title = serializers.CharField(validators=[validators.validate_title_no_hello, validators.unique_product_title])
    # you can put the validator straight to model, that's best if you can, and you can redeclare here what it should be
    body = serializers.CharField(source='content')

    class Meta:
        model = Product
        fields = [
            'owner',
            'pk',
            'title',
            'body',
            'price',
            'sale_price',
            'public',
            'path',
            'endpoint',
        ]

    def get_my_user_data(self, obj):
        return {
            "username": obj.user.username
        }

    def get_edit_url(self, obj):
        request = self.context.get('request')  # get the request like this as it' maybe None
        if request is None:
            return None
        return reverse("product-edit", kwargs={"pk": obj.pk}, request=request)


